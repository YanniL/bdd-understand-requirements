package co.Chapter_2;
import java.util.Arrays;
import java.util.List;

import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;
 
import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class FindStories extends JUnitStories{
	public FindStories() {
		super();
	}
 
	@Override
	public InjectableStepsFactory stepsFactory() {
		return new InstanceStepsFactory(configuration(), new OptimalItinerarySteps());
	}
 
	@Override
	protected List<String> storyPaths() {

		return Arrays.asList("**/find*.story");
	}
}
















